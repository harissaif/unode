# README #
package.json Dependencies?
mongodb (database) 
mongoose (a driver to interact with MongoDB in a simplified way) 
connect-flash (allow us to send flash messages over a session e.g after login->send message->"you are logged in" and save it in the memory) 
express-validator (for form validation) 
express-session (sessions are used in this project) 
express-messages(to create messages to spit out in .jade template) 
passport (user authentication helper e.g create user session and save session data etc.) 
passport-local (where we write our own functions. "passport-facebook", "passport-twitter" in case you want to login through fb etc.) 
passport-http (To send http requests) 
multer (is a module that simplifies files uploards to the server, get information on the files, location, file sizes etc.)
npm install ;)

require them in the app.js 
var session = require('express-session'); 
var passport = require('passport'); 
var LocalStrategy = require('passport-local').Strategy; 
var bodyParser = require('body-parser'); 
var multer = require('multer'); 
var flash = require('connect-flash'); 
var mongo = require('mongodb'); 
var mongoose = require('mongoose'); 
var db = mongoose.connection;